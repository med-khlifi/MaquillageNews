<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BeautyParadise\BackEndBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Description of ArticleController
 *
 * @author user
 */
class HomeController extends Controller {
    public function addArticleAction()        
    {
        
        
        return $this->render('BeautyParadiseBackEndBundle:ArticleTwig:AddArticle.html.twig');
    }
      public function indexAction()        
    {
           $em = $this->getDoctrine()->getManager();
             $connection = $em->getConnection();
     $statement1 = $connection->prepare("SELECT *
FROM produit

WHERE Coup_coeur = 1 ORDER BY id DESC;
");

$statement1->execute();
$produits = $statement1->fetchAll();
         $Tutos= $em->getRepository('BeautyParadiseBackEndBundle:Tuto')->findAll();  
        $News= $em->getRepository('BeautyParadiseBackEndBundle:News')->findAll();
        $articles= $em->getRepository('BeautyParadiseBackEndBundle:Article')->findAll();
        return $this->render('BeautyParadiseBackEndBundle:Default:Home.html.twig', array('news' => $News,'articles' => $articles,'produits' => $produits,'Tutos' => $Tutos));
    }
    public function createArticleAction(Request $request) {
        $modele = new \BeautyParadise\BackEndBundle\Entity\Article;

        $Request = $this->getRequest();

        $date1 = $Request->get('date');
        $auteur = $Request->get('auteur');
        $contenu = $Request->get('contenu');
        $titre = $Request->get('titre');
        $video = $Request->get('video');
      $photo2 = $request->files->get('photo2')->getClientOriginalName();
      $photo1 = $request->files->get('photo1')->getClientOriginalName();
        $date = new \DateTime();
        $timestamp = $date->format('U');
        $isUploaded = false;
        //$filename = "";
        var_dump($date1);
        var_dump($request->files->get('photo2')->getClientOriginalName());
        var_dump($request->files->get('photo1')->getClientOriginalName());
        var_dump($request->files);
        try {
            
                $filename = "" . $timestamp . basename($photo1);
                $filename = preg_replace('/\s+/', '_', $filename);
                $path = $this->get('kernel')->getRootDir() . "/../web/uploads/Article/" . $filename;
                if (move_uploaded_file($request->files->get('photo1'), $path)) {
                    $isUploaded = true;
                } else {
                    return new Response(Response::HTTP_404);
                }
               $filename1 = "" . $timestamp . basename($photo2);
                $filename1 = preg_replace('/\s+/', '_', $filename1);
                $path1 = $this->get('kernel')->getRootDir() . "/../web/uploads/Article/" . $filename1;
                if (move_uploaded_file($request->files->get('photo2'), $path1)) {
                    $isUploaded = true;
                } else {
                    return new Response(Response::HTTP_404);
                }
        } catch (Exception $e) {
            var_dump($e);        }
 var_dump($isUploaded);
        if ($isUploaded) {
        $modele->setTitre($titre);
        $modele->setDate($date1);
        $modele->setContenu($contenu);
        $modele->setImage("http://127.0.0.1/beauty_paradise/web/uploads/Article/" .$filename);
        $modele->setAuthor($auteur);
        $modele->setAuthorimg("http://127.0.0.1/beauty_paradise/web/uploads/Article/" .$filename1);
        $modele->setHates(0);
        $modele->setLikes(0);
        $modele->setVideo($video);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele);
        
        $em->flush();
        }
       
       return $this->redirect($this->generateUrl('Article'));
    }
    public function supprimerArticleAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $evenement = $em->getRepository('BeautyParadiseBackEndBundle:Article')->find($id);
        $em->remove($evenement);
        $em->flush();
        // return new Response("suppression avec succès");

       
        return $this->redirect($this->generateUrl('Article'));
    }
    
        public function updateArticleAction($id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $em->getRepository('BeautyParadiseBackEndBundle:Article')->find($id);
        $form = $this->createform(new \BeautyParadise\BackEndBundle\Form\ArticleType(), $user);
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->get('doctrine')->getEntityManager();
                $em->persist($user);
                $em->flush();
               
               return $this->redirect($this->generateUrl('Article'));
            }
        }


        return $this->render('BeautyParadiseBackEndBundle:ArticleTwig:UpdateArticle.html.twig', array('form' => $form->createView(), 'id' => $id));
    }
}
