<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BeautyParadise\BackEndBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Description of ArticleController
 *
 * @author user
 */
class ProduitController extends Controller {
    public function addProduitAction()        
    {
        
        
        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:AddProduit.html.twig');
    }
    

      public function RechercheProduitAction(Request $request)        
    {
         $Request = $this->getRequest();
         $mot_cle = $Request->get('text');
          $categorie = $Request->get('text2');
       
                           $em = $this->getDoctrine()->getManager();
     $connection = $em->getConnection();
$statement1 = $connection->prepare("SELECT * FROM produit
WHERE nom LIKE '%".$mot_cle."%' AND categorie ='".$categorie."' ;");

$statement1->execute();
$produit = $statement1->fetchAll();
 $msg=false;
  if ( empty($produit)) {
  $msg=true;
   $em = $this->getDoctrine()->getManager();

        $articles= $em->getRepository('BeautyParadiseBackEndBundle:Produit')->findBy(array('categorie' => $categorie));
        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:ListeProduit.html.twig', array('articles' => $articles,'test' => $msg));
} else {
    $msg=false;
      return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:ListeProduit.html.twig', array('articles' => $produit,'test' => $msg));
}

      
    }
    
    
     public function RechercheProduit2Action(Request $request)        
    {
         $Request = $this->getRequest();
         $mot_cle = $Request->get('radio');
          $categorie = $Request->get('text2');

                           $em = $this->getDoctrine()->getManager();
     $connection = $em->getConnection();
$statement1 = $connection->prepare("SELECT * FROM produit
WHERE type_peau = '".$mot_cle."' AND categorie ='".$categorie."' ;");

$statement1->execute();
$produit = $statement1->fetchAll();
 $msg=false;
if ( empty($produit)) {
   $msg=true;
   $em = $this->getDoctrine()->getManager();

        $articles= $em->getRepository('BeautyParadiseBackEndBundle:Produit')->findBy(array('categorie' => $categorie));
        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:ListeProduit.html.twig', array('articles' => $articles,'test' => $msg));
} else {
     $msg=false;
      return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:ListeProduit.html.twig', array('articles' => $produit,'test' => $msg));
}

      
    }
    
    
        public function ListeProduitAction($type)        
    {
           $em = $this->getDoctrine()->getManager();
 $msg=false;
        $articles= $em->getRepository('BeautyParadiseBackEndBundle:Produit')->findBy(array('categorie' => $type));
      if ( empty($articles)) {
           $msg=true;
            return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:ListeProduit.html.twig', array('articles' => $articles,'test' => $msg));
      }
        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:ListeProduit.html.twig', array('articles' => $articles,'test' => $msg));
    }
         public function indexAction()        
    {
                  $em = $this->getDoctrine()->getManager();
     $connection = $em->getConnection();
$statement1 = $connection->prepare("SELECT COUNT(*) as nbr FROM news  WHERE 1=1 ");
$statement1->execute();
$news = $statement1->fetchAll();
  $statement2 = $connection->prepare("SELECT COUNT(*) as nbr FROM article  WHERE 1=1 ");
$statement2->execute();
$article = $statement2->fetchAll();
$statement3 = $connection->prepare("SELECT COUNT(*) as nbr FROM produit  WHERE 1=1 ");
$statement3->execute();
$produit = $statement3->fetchAll();
$statement4 = $connection->prepare("SELECT COUNT(*) as nbr FROM adimn  WHERE 1=1 ");
$statement4->execute();
$admins = $statement4->fetchAll();
        $articles= $em->getRepository('BeautyParadiseBackEndBundle:Produit')->findAll();
        
        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:Produit.html.twig', array('articles' => $articles,'admins' => $admins,'news' => $news,'article' => $article,'produit' => $produit));
    }
    public function createProduitAction(Request $request) {
        $modele = new \BeautyParadise\BackEndBundle\Entity\Produit;

        $Request = $this->getRequest();

        $date1 = $Request->get('date');
        $nom = $Request->get('Nom');
         $prix= $Request->get('prix');
        $Description = $Request->get('Description');
        $Video = $Request->get('Video');
$categorie = $Request->get('categorie');
$SousCategorie = $Request->get('Sous-Categorie');
$Typedepeau = $Request->get('Type-de-peau');
$Typeteint = $Request->get('Type-teint');
      $photo1 = $request->files->get('photo1')->getClientOriginalName();
        $date = new \DateTime();
        $timestamp = $date->format('U');
        $isUploaded = false;
        //$filename = "";
        var_dump($date1);

        var_dump($request->files->get('photo1')->getClientOriginalName());
        var_dump($request->files);
        try {
            
                $filename = "" . $timestamp . basename($photo1);
                $filename = preg_replace('/\s+/', '_', $filename);
                $path = $this->get('kernel')->getRootDir() . "/../web/uploads/Produit/" . $filename;
                if (move_uploaded_file($request->files->get('photo1'), $path)) {
                    $isUploaded = true;
                } else {
                    return new Response(Response::HTTP_404);
                }
               
        } catch (Exception $e) {
            var_dump($e);        }
 var_dump($isUploaded);
        if ($isUploaded) {
        $modele->setDescription($Description);
        $modele->setCategorie($categorie);
        $modele->setNom($nom);
        $modele->setImage("http://127.0.0.1/beauty_paradise/web/uploads/Produit/" .$filename);
        $modele->setPrix($prix);
         $modele->setRate(0);
          $modele->setSousCategorie($SousCategorie);
          $modele->setTypePeau($Typedepeau);
          $modele->setTypeTient($Typeteint);
   $modele->setVideo($Video);
   $modele->setRatedby(0);
   $modele->setAllrate(0);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele);
        $em->flush();
        }
       
       return $this->redirect($this->generateUrl('Produit'));
    }
    public function supprimerProduitAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $evenement = $em->getRepository('BeautyParadiseBackEndBundle:Produit')->find($id);
        $em->remove($evenement);
        $em->flush();
        // return new Response("suppression avec succès");

       
        return $this->redirect($this->generateUrl('Produit'));
    }
    
        public function updateProduitAction($id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $em->getRepository('BeautyParadiseBackEndBundle:Produit')->find($id);
        $form = $this->createform(new \BeautyParadise\BackEndBundle\Form\ProduitType(), $user);
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->get('doctrine')->getEntityManager();
                $em->persist($user);
                $em->flush();
               
               return $this->redirect($this->generateUrl('Produit'));
            }
        }


        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:UpdateProduit.html.twig', array('form' => $form->createView(), 'id' => $id));
    }
    
    public function listerAction()
{
	// ...
	
	
	return $this->container->get('templating')->renderResponse('MyAppFilmothequeBundle:Acteur:lister.html.twig', array(
		'Produit' => $Produit,
		'form' => $form->createView()
	));
}

   public function SingleProduitAction($id)        
    {
        
         $em = $this->getDoctrine()->getManager();

        $aa= $em->getRepository('BeautyParadiseBackEndBundle:Produit')->find($id);
       
        $Tuto= $em->getRepository('BeautyParadiseBackEndBundle:Tuto')->findAll();
           $Produit= $em->getRepository('BeautyParadiseBackEndBundle:Produit')->findAll();
        return $this->render('BeautyParadiseBackEndBundle:ProduitTwig:SingelProduit.html.twig', array('aa' => $aa,'Produits' => $Produit,'tuto' => $Tuto));
       
    }
}
