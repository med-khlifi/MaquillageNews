<?php

namespace BeautyParadise\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()        
    {
      
        
        return $this->render('BeautyParadiseBackEndBundle:Default:Home.html.twig');
    }
}
