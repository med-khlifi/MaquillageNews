<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BeautyParadise\BackEndBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Description of ArticleController
 *
 * @author user
 */
class NewsController extends Controller {
    public function addNewsAction()        
    {
        
        
        return $this->render('BeautyParadiseBackEndBundle:NewsTwig:AddNews.html.twig');
    }
       public function SingleNewsAction($id)        
    {
        
         $em = $this->getDoctrine()->getManager();

        $aa= $em->getRepository('BeautyParadiseBackEndBundle:News')->find($id);
       
        $news= $em->getRepository('BeautyParadiseBackEndBundle:News')->findAll();
           $articles= $em->getRepository('BeautyParadiseBackEndBundle:Article')->findAll();
        return $this->render('BeautyParadiseBackEndBundle:NewsTwig:SingelNews.html.twig', array('aa' => $aa,'news' => $news,'articles' => $articles));
       
    }
    
        public function ListeNewsAction()        
    {
           $em = $this->getDoctrine()->getManager();

        $articles= $em->getRepository('BeautyParadiseBackEndBundle:News')->findAll();
        return $this->render('BeautyParadiseBackEndBundle:NewsTwig:ListeNews.html.twig', array('articles' => $articles));
    }
    
         public function indexAction()        
    {
                  $em = $this->getDoctrine()->getManager();
     $connection = $em->getConnection();
$statement1 = $connection->prepare("SELECT COUNT(*) as nbr FROM news  WHERE 1=1 ");
$statement1->execute();
$news = $statement1->fetchAll();
  $statement2 = $connection->prepare("SELECT COUNT(*) as nbr FROM article  WHERE 1=1 ");
$statement2->execute();
$article = $statement2->fetchAll();
$statement3 = $connection->prepare("SELECT COUNT(*) as nbr FROM produit  WHERE 1=1 ");
$statement3->execute();
$produit = $statement3->fetchAll();
$statement4 = $connection->prepare("SELECT COUNT(*) as nbr FROM adimn  WHERE 1=1 ");
$statement4->execute();
$admins = $statement4->fetchAll();
        $articles= $em->getRepository('BeautyParadiseBackEndBundle:News')->findAll();
        
        return $this->render('BeautyParadiseBackEndBundle:NewsTwig:News.html.twig', array('articles' => $articles,'admins' => $admins,'news' => $news,'article' => $article,'produit' => $produit));
    }
    public function createNewsAction(Request $request) {
        $modele = new \BeautyParadise\BackEndBundle\Entity\News;

        $Request = $this->getRequest();

        $date1 = $Request->get('date');
        $source = $Request->get('source');
        $contenu = $Request->get('contenu');
        $titre = $Request->get('titre');

      $photo1 = $request->files->get('photo1')->getClientOriginalName();
        $date = new \DateTime();
        $timestamp = $date->format('U');
        $isUploaded = false;
        //$filename = "";
        var_dump($date1);

        var_dump($request->files->get('photo1')->getClientOriginalName());
        var_dump($request->files);
        try {
            
                $filename = "" . $timestamp . basename($photo1);
                $filename = preg_replace('/\s+/', '_', $filename);
                $path = $this->get('kernel')->getRootDir() . "/../web/uploads/News/" . $filename;
                if (move_uploaded_file($request->files->get('photo1'), $path)) {
                    $isUploaded = true;
                } else {
                    return new Response(Response::HTTP_404);
                }
               
        } catch (Exception $e) {
            var_dump($e);        }
 var_dump($isUploaded);
        if ($isUploaded) {
        $modele->setTitre($titre);
        $modele->setDate($date1);
        $modele->setContenu($contenu);
        $modele->setImage("http://127.0.0.1/beauty_paradise/web/uploads/News/" .$filename);
        $modele->setSource($source);
   
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele);
        $em->flush();
        }
       
       return $this->redirect($this->generateUrl('News'));
    }
    public function supprimerNewsAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $evenement = $em->getRepository('BeautyParadiseBackEndBundle:News')->find($id);
        $em->remove($evenement);
        $em->flush();
        // return new Response("suppression avec succès");

       
        return $this->redirect($this->generateUrl('News'));
    }
    
        public function updateNewsAction($id) {


        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $em->getRepository('BeautyParadiseBackEndBundle:News')->find($id);
        $form = $this->createform(new \BeautyParadise\BackEndBundle\Form\NewsType(), $user);
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->get('doctrine')->getEntityManager();
                $em->persist($user);
                $em->flush();
               
               return $this->redirect($this->generateUrl('News'));
            }
        }


        return $this->render('BeautyParadiseBackEndBundle:NewsTwig:UpdateNews.html.twig', array('form' => $form->createView(), 'id' => $id));
    }
}
