<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BeautyParadise\BackEndBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Description of ArticleController
 *
 * @author user
 */
class TutoController extends Controller {
    public function addTutoAction()        
    {
        
        
        return $this->render('BeautyParadiseBackEndBundle:TutoTwig:AddTuto.html.twig');
    }
    
     
      public function indexAction()        
    {
           $em = $this->getDoctrine()->getManager();
     $connection = $em->getConnection();
$statement1 = $connection->prepare("SELECT COUNT(*) as nbr FROM news  WHERE 1=1 ");
$statement1->execute();
$news = $statement1->fetchAll();
  $statement2 = $connection->prepare("SELECT COUNT(*) as nbr FROM article  WHERE 1=1 ");
$statement2->execute();
$article = $statement2->fetchAll();
$statement3 = $connection->prepare("SELECT COUNT(*) as nbr FROM produit  WHERE 1=1 ");
$statement3->execute();
$produit = $statement3->fetchAll();
$statement4 = $connection->prepare("SELECT COUNT(*) as nbr FROM adimn  WHERE 1=1 ");
$statement4->execute();
$admins = $statement4->fetchAll();
        $articles= $em->getRepository('BeautyParadiseBackEndBundle:Tuto')->findAll();
        
        return $this->render('BeautyParadiseBackEndBundle:TutoTwig:Tuto.html.twig', array('articles' => $articles,'admins' => $admins,'news' => $news,'article' => $article,'produit' => $produit));
    }
    public function createTutoAction(Request $request) {
        $modele = new \BeautyParadise\BackEndBundle\Entity\Tuto();

        $Request = $this->getRequest();

        $video = $Request->get('video');


  


        $modele->setVideo($video);
        $em = $this->getDoctrine()->getManager();
        $em->persist($modele);
        
        $em->flush();
        
       
       return $this->redirect($this->generateUrl('Tuto'));
    }
    public function supprimerArticleAction($id) {

        $em = $this->container->get('doctrine')->getEntityManager();
        $evenement = $em->getRepository('BeautyParadiseBackEndBundle:Tuto')->find($id);
        $em->remove($evenement);
        $em->flush();
        // return new Response("suppression avec succès");

       
        return $this->redirect($this->generateUrl('Tuto'));
    }
    
       
}
