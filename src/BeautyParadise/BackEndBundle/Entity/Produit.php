<?php

namespace BeautyParadise\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity
 */
class Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string")
     */
    private $video;

     /**
     * @var integer
     *
     * @ORM\Column(name="rate", type="integer")
     */
    private $rate;
    /**
     * @var integer
     *
     * @ORM\Column(name="ratedby", type="integer")
     */
    private $ratedby;
    /**
     * @var integer
     *
     * @ORM\Column(name="allrate", type="integer")
     */
    private $allrate;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="sous_categorie", type="string")
     */
    private $sousCategorie;

    /**
     * @var string
     *
     * @ORM\Column(name="type_peau", type="string")
     */
    private $typePeau;

    /**
     * @var string
     *
     * @ORM\Column(name="type_tient", type="string")
     */
    private $typeTient;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    function getRatedby() {
        return $this->ratedby;
    }

    function getAllrate() {
        return $this->allrate;
    }

    function setRatedby($ratedby) {
        $this->ratedby = $ratedby;
    }

    function setAllrate($allrate) {
        $this->allrate = $allrate;
    }

        /**
     * Set description
     *
     * @param string $description
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Produit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Produit
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return Produit
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     * @return Produit
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set sousCategorie
     *
     * @param string $sousCategorie
     * @return Produit
     */
    public function setSousCategorie($sousCategorie)
    {
        $this->sousCategorie = $sousCategorie;

        return $this;
    }

    /**
     * Get sousCategorie
     *
     * @return string 
     */
    public function getSousCategorie()
    {
        return $this->sousCategorie;
    }

    /**
     * Set typePeau
     *
     * @param string $typePeau
     * @return Produit
     */
    public function setTypePeau($typePeau)
    {
        $this->typePeau = $typePeau;

        return $this;
    }

    /**
     * Get typePeau
     *
     * @return string 
     */
    public function getTypePeau()
    {
        return $this->typePeau;
    }

    /**
     * Set typeTient
     *
     * @param string $typeTient
     * @return Produit
     */
    public function setTypeTient($typeTient)
    {
        $this->typeTient = $typeTient;

        return $this;
    }

    /**
     * Get typeTient
     *
     * @return string 
     */
    public function getTypeTient()
    {
        return $this->typeTient;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Produit
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}
