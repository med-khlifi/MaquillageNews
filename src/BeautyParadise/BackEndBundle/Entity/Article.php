<?php

namespace BeautyParadise\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="likes", type="integer")
     */
    private $likes;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="hates", type="integer")
     */
    private $hates;
   
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string")
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string")
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string")
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="authorimg", type="string")
     */
    private $authorimg;
    
     /**
     * @var string
     *
     * @ORM\Column(name="video", type="string")
     */
    private $video;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }
    function getLikes() {
        return $this->likes;
    }

    function getHates() {
        return $this->hates;
    }

    function getVideo() {
        return $this->video;
    }

    function setLikes($likes) {
        $this->likes = $likes;
    }

    function setHates($hates) {
        $this->hates = $hates;
    }

    function setVideo($video) {
        $this->video = $video;
    }

        /**
     * Set date
     *
     * @param string $date
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set authorimg
     *
     * @param string $authorimg
     * @return Article
     */
    public function setAuthorimg($authorimg)
    {
        $this->authorimg = $authorimg;

        return $this;
    }

    /**
     * Get authorimg
     *
     * @return string 
     */
    public function getAuthorimg()
    {
        return $this->authorimg;
    }
}
