<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BeautyParadise\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="tuto")
 */
class Tuto 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
     /**
     * @var string
     *
     * @ORM\Column(name="video", type="string")
     */
    private $video;
    
    function getVideo() {
        return $this->video;
    }

    function setVideo($video) {
        $this->video = $video;
    }

        
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }


}